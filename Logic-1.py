def cigar_party(cigars, is_weekend):
    if is_weekend == True and cigars >= 40:
        return True
    elif is_weekend == False and cigars >= 40 and cigars <= 60:
        return True
    else:
        return False


def caught_speeding(speed, is_birthday):
    if is_birthday == False:
        if speed <= 60:
            return 0
        elif speed >= 61 and speed <= 80:
            return 1
        elif speed >= 81:
            return 2
    elif is_birthday == True:
        if speed <= 65:
            return 0
        elif speed >= 66 and speed <= 85:
            return 1
        elif speed >= 86:
            return 2


def sorta_sum(a, b):
    sum = a + b
    if sum >= 10 and sum <= 19:
        return 20
    else:
        return sum


def alarm_clock(day, vacation):
    if vacation == False:
        if day == 6 or day == 0:
            return "10:00"
        else:
            return "7:00"
    if vacation == True:
        if day == 6 or day == 0:
            return "off"
        else:
            return "10:00"


def love6(a, b):
    if a == 6 or b == 6:
        return True
    elif a + b == 6 or abs(a - b) == 6:
        return True
    else:
        return False


def in1to10(n, outside_mode):
    if not outside_mode and n >= 1 and n <= 10:
        return True
    elif outside_mode and (n <= 1 or n >= 10):
        return True
    else:
        return False


def squirrel_play(temp, is_summer):
    if is_summer and temp >= 60 and temp <= 100:
        return True
    elif not is_summer and temp >= 60 and temp <= 90:
        return True
    else:
        return False


def near_ten(num):
    if num % 10 <= 2 or num % 10 >= 8:
        return True
    else:
        return False


def date_fashion(you, date):
    if you <= 2 or date <= 2:
        return 0
    elif you >= 8 or date >= 8:
        return 2
    else:
        return 1
