def sleep_in(weekday, vacation):
    if weekday == False or vacation == True:
        return True
    else:
        return False


def diff21(n):
  if n <= 21:
    return 21 - n
  else:
    return abs(21 - n) * 2


def near_hundred(n):
    if abs(100 - n) <= 10 or abs(200 - n) <= 10:
        return True
    else:
        return False


def missing_char(str, n):
    return str[:n] + str[n + 1:]


def monkey_trouble(a_smile, b_smile):
  if a_smile == b_smile:
    return True
  else:
    return False


def parrot_trouble(talking, hour):
  if  talking and (hour < 7 or hour > 20):
    return True
  else:
    return False


def pos_neg(a, b, negative):
    if negative == True and (a < 0 and b < 0):
        return True
    elif negative == False and ((a < 0 and b > 0) or (a > 0 and b < 0)):
        return True
    else:
        return False


def front_back(str):
    if len(str) <= 1:
        return str
    return str[-1] + str[1:-1] + str[0]


def sum_double(a, b):
  if a == b:
    return (a + b)*2
  else:
    return a + b


def makes10(a, b):
  if a == 10 or b == 10 or a + b == 10:
    return True
  else:
    return False


def not_string(str):
    if str[:3] == "not":
        return str
    else:
        return "not " + str


def front3(str):
    return "%s%s%s" % (str[:3], str[:3], str[:3])
