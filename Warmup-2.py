def string_times(str, n):
    result = ""
    for i in range(n):
        result += str
    return result


def string_bits(str):
    result = ""
    for i in range(0, len(str), 2):
        result += str[i]
    return result


def front_times(str, n):
    str_new = ""
    for i in range(n):
        str_new += str[:3]
    return str_new


def string_splosion(str):
    str_new = ""
    for i in range(len(str)):
        str_new += str[:i + 1]
    return str_new


def array_front9(nums):
    for i in range(len(nums)):
        if i == 4:
            break
        if nums[i] == 9:
            return True
    return False


def array123(nums):
    for i in range(len(nums) - 2):
        if nums[i] == 1 and nums[i + 1] == 2 and nums[i + 2] == 3:
            return True
    return False


def array_count9(nums):
    count = 0
    for i in nums:
        if i == 9:
            count += 1
    return count


def string_match(a, b):
    arr_size = min(len(a), len(b))
    count = 0
    for i in range(arr_size - 1):
        a_ = a[i:i + 2]
        b_ = b[i:i + 2]
        if a_ == b_:
            count += 1
    return count


def last2(str):
    count = 0
    for i in range(len(str) - 2):
        if str[i:i + 2] == str[-2:]:
            count += 1
    return count
