def first_last6(nums):
    if nums[0] == 6 or nums[-1] == 6:
        return True
    else:
        return False


def same_first_last(nums):
    if len(nums) and nums[0] == nums[-1]:
        return True
    else:
        return False


def common_end(a, b):
    if a[0] == b[0] or a[-1] == b[-1]:
        return True
    else:
        return False


def sum3(nums):
    sum = 0
    for i in nums:
        sum += i
    return sum


def sum2(nums):
    if len(nums) >= 2:
        return nums[0] + nums[1]
    elif len(nums) == 1:
        return nums[0]
    else:
        return 0


def max_end3(nums):
    if nums[0] > nums[-1]:
        arr_new = [nums[0], nums[0], nums[0]]
    else:
        arr_new = [nums[-1], nums[-1], nums[-1]]
    return arr_new


def middle_way(a, b):
    arr_new = [a[1], b[1]]
    return arr_new


def make_ends(nums):
    arr_new = [nums[0], nums[-1]]
    return arr_new


def has23(nums):
    result = False
    for i in range(len(nums)):
        if nums[i] == 2 or nums[i] == 3:
            result = True
    return result


def rotate_left3(nums):
    arr_new = [nums[1], nums[2], nums[0]]
    return arr_new


def reverse3(nums):
    arr_new = [nums[-1], nums[1], nums[0]]
    return arr_new


def make_pi():
    ip = [3, 1, 4]
    return ip


