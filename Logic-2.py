def lone_sum(a, b, c):
    if a != b and a != c and b != c:
        return a + b + c
    if a == b and a == c:
        return 0
    if a == b:
        return c
    if a == c:
        return b
    if b == c:
        return a


def lucky_sum(a, b, c):
    if a != 13 and b != 13 and c != 13:
        return a + b + c
    if a == 13:
        return 0
    if b == 13:
        return a
    if c == 13:
        return a + b


def close_far(a, b, c):
    if abs(a - b) <= 1 and abs(a - c) >= 2 and abs(c - b) >= 2:
        return True
    elif abs(a - c) <= 1 and abs(a - b) >= 2 and abs(c - b) >= 2:
        return True
    elif abs(b - c) <= 1 and abs(a - b) >= 2 and abs(c - a) >= 2:
        return True
    else:
        return False


def round_sum(a, b, c):
    return round10(a) + round10(b) + round10(c)


def round10(num):
    if num % 10 >= 5:
        return (num - num % 10) + 10
    else:
        return num - num % 10


def no_teen_sum(a, b, c):
    return fix_teen(a) + fix_teen(b) + fix_teen(c)


def fix_teen(n):
    if n >= 13 and n <= 19:
        if n == 15 or n == 16:
            return n
        else:
            return 0
    else:
        return n


def make_bricks(small, big, goal):
    if big == 0:
        big_count = 0
    elif goal // 5 > big:
        big_count = big
    else:
        big_count = goal // 5

    if goal - (big_count * 5) <= small:
        return True
    else:
        return False


def make_chocolate(small, big, goal):
    if big == 0:
        big_count = 0
    elif goal // 5 > big:
        big_count = big
    else:
        big_count = goal // 5

    if goal - (big_count * 5) > small:
        return -1
    else:
        return goal - (big_count * 5)
