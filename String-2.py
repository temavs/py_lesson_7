def double_char(str):
    str_new = ""
    for i in range(len(str)):
        str_new += str[i] + str[i]
    return str_new


def count_hi(str):
    count = 0
    for i in range(len(str) - 1):
        if str[i:i + 2] == "hi":
            count += 1
    return count


def end_other(a, b):
    if a[-2:].lower() == b[-2:].lower() and len(a) >= 2 and len(b) >= 2:
        return True
    elif a[-1:].lower() == b[-1:].lower() and (len(a) <= 1 or len(b) <= 1):
        return True
    else:
        return False


def cat_dog(str):
    count_cat = 0
    count_dog = 0
    for i in range(len(str) - 2):
        if str[i:i + 3] == "cat":
            count_cat += 1

    for i in range(len(str) - 2):
        if str[i:i + 3] == "dog":
            count_dog += 1

    if count_cat == count_dog:
        return True
    else:
        return False


def count_code(str):
    count = 0
    for i in range(len(str) - 3):
        if str[i:i + 2] == "co" and str[i + 3: i + 4] == "e":
            count += 1
    return count


def xyz_there(str):
    if len(str) < 3:
        return False

    for i in range(len(str) - 2):
        if str[i: i + 3] == "xyz" and str[i - 1] != ".":
            return True
    return False
