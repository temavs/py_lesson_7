def sum13(nums):
    sum = 0
    if len(nums) <= 1:
        return 0

    for i in range(len(nums)):
        if nums[i] == 13:
            continue
        elif i != 0 and nums[i - 1] == 13:
            continue
        else:
            sum += nums[i]

    return sum


def count_evens(nums):
    count = 0
    for i in nums:
        if i % 2 == 0:
            count += 1

    return count


def big_diff(nums):
    min_ = min(nums)
    max_ = max(nums)
    return max_ - min_


def has22(nums):
    for i in range(len(nums) - 1):
        if nums[i] == 2 and nums[i + 1] == 2:
            return True
    return False


def centered_average(nums):
    min_ = min(nums)
    max_ = max(nums)
    nums.remove(min_)
    nums.remove(max_)
    count = len(nums)
    sum = 0
    for i in range(count):
        sum += nums[i]
    return sum / count


def sum67(nums):
    sum = 0
    counter = True

    for i in nums:
        if i == 6:
            counter = False
        elif not counter and i == 7:
            counter = True
            continue

        if counter == True:
            sum += i

    return sum
